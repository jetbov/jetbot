
.DEFAULT_GOAL = help

CONTAINER_NAME = "jetbot"
IMAGE_NAME = "jetbov/jetbot"

# The @ makes sure that the command itself isn't echoed in the terminal
help:
	@echo " "
	@echo "Jetbovis - Backend de relatórios"
	@echo " "
	@echo "  make build   : Para gerar a imagem docker"
	@echo "  make run     : Para rodar a primeira vez a imagem docker"
	@echo "  make start   : Para rodar a imagem docker"
	@echo "  make restart : Para reiniciar a imagem docker"
	@echo "  make logs    : Para acompanhar os logs da instancia"
	@echo "  make delete  : Para parar e remover o container do tarefas"
	@echo " "

build:
	docker build . -t ${IMAGE_NAME}

start:
	docker start ${CONTAINER_NAME}

stop:
	docker stop ${CONTAINER_NAME}

restart:
	docker restart ${CONTAINER_NAME}

logs:
	docker logs -f --tail=150 ${CONTAINER_NAME}

delete:
	docker stop ${CONTAINER_NAME} && docker rm ${CONTAINER_NAME}

run:
	docker run --env-file local.env -d --name ${CONTAINER_NAME} ${IMAGE_NAME}

shell:
	docker run -it --rm --name ${CONTAINER_NAME} ${IMAGE_NAME} /bin/bash

shell2:
	docker exec -it ${CONTAINER_NAME} /bin/bash
