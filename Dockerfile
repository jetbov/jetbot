FROM python:3.9-slim

COPY requirements.txt requirements.txt

RUN apt-get update

RUN apt-get install --no-install-recommends --no-upgrade -y locales

RUN apt-get update && rm /etc/localtime && ln -s /usr/share/zoneinfo/America/Sao_Paulo /usr/localtime && \
    echo America/Sao_Paulo > /etc/timezone && \
    sed -i -e 's/# pt_BR.UTF-8 UTF-8/pt_BR.UTF-8 UTF-8/' /etc/locale.gen && locale-gen && \
    dpkg-reconfigure -f noninteractive tzdata locales && \
    update-locale && \
    python -m pip install --upgrade pip 

RUN pip install --no-cache-dir -r requirements.txt

COPY ./bots /jetbot
WORKDIR /jetbot

CMD python telegram_api.py