import json
import requests
import locale
from datetime import datetime

from queries.lote_queries import (
    sql_cotacao,
    sql_peso
)

locale.setlocale(locale.LC_ALL, 'pt_BR.UTF-8')


def msg_pesos(farm_id, farm_name):

    cotacao_tuple = sql_cotacao()[0]
    data = cotacao_tuple[0].strftime("%d/%m/%Y")
    cotacao = cotacao_tuple[1]

    lotes = sql_peso(farm_id)

    msg = f'{farm_id} - <b>{farm_name}</b>\n'
    msg += f'@ BOI ESALQ/BMF ({data}):  <b>R$ {cotacao}</b>\n\n'
    msg += 'Lote; Peso médio;\n (Nr animais);  peso total;  Cotação\n'
    msg += '--------------------------------------------\n'
    for lote in lotes:
        name = lote[1]
        animais = lote[2]
        peso = lote[3] or 0.0
        peso = float(peso)
        peso = round(peso, 2)
        peso = locale.currency(peso, grouping=True, symbol=None)
        valor = 0
        arrobas = lote[4]
        medio = lote[5]
        medio = float(medio)
        medio = round(medio, 2)
        medio = locale.currency(medio, grouping=True, symbol=None)
        if arrobas:
            valor = arrobas * cotacao
            valor = locale.currency(valor, grouping=True, symbol=None)
        valor = f'R$ {valor}'
        msg += f'{name};  {medio} kg\n({animais}): {peso} Kg, {valor}\n\n'
    msg += '/lotes: Informações dos lotes\n\n'
    msg += '/inicial: Volta para o menu inicial'
    return msg
