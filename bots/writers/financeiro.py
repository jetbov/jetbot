import json

import locale
from datetime import datetime

from queries.financeiro_queries import sql_saldo, sql_movimentos

locale.setlocale(locale.LC_ALL, 'pt_BR.UTF-8')

def msg_saldo(farm_id, farm_name):

    data = datetime.today().strftime("%d/%m/%Y")
    contas = sql_saldo(farm_id)

    msg = f'{farm_id} - <b>{farm_name}</b>\n'
    msg += f'{data}\n'
    msg += 'Conta   saldo\n'
    msg += '--------------------------------------------\n'
    for conta in contas:
        name = conta.get('account_name')
        valor = round(conta.get('balance'), 2)
        valor = locale.currency(valor, grouping=True, symbol=None)
        valor = f'R$ {valor}'
        msg += f'{name}  {valor}\n\n'
    msg += '/financeiro: Informações financeiras\n\n'
    msg += '/inicial: Volta para o menu inicial'
    return msg

def msg_prx_pagamentos(farm_id, farm_name):

    data = datetime.today().strftime("%d/%m/%Y")

    movimentos = sql_movimentos(farm_id, 'payment')
    nr = len(movimentos)

    msg = f'{farm_id} - <b>{farm_name}</b>\n'
    msg += f'{data}\n'
    msg += f'Próximos {nr} (-)Pagamentos\n'
    msg += 'Referência, (Valor), Data\n'
    msg += '--------------------------------------------\n'
    if movimentos:
        for r in movimentos:
            ref = r[0]
            valor = float(r[1])
            valor = round(valor, 2)
            valor = locale.currency(valor, grouping=True, symbol=None)
            data = r[2].strftime("%d/%m/%Y")
            msg += f'{ref}:\n  (R${valor}) {data}\n\n'
    else:
        msg += 'Não há Pagamentos futuros\n'

    msg += '/financeiro: Informações financeiras\n\n'
    msg += '/inicial: Volta para o menu inicial'
    return msg

def msg_prx_recebimentos(farm_id, farm_name):

    data = datetime.today().strftime("%d/%m/%Y")
    movimentos = sql_movimentos(farm_id, 'receipt')
    nr = len(movimentos)

    msg = f'{farm_id} - <b>{farm_name}</b>\n'
    msg += f'{data}\n'
    msg += f'Próximos {nr} (+)Recebimentos\n'
    msg += 'Referência, Valor, Data\n'
    msg += '--------------------------------------------\n'
    if movimentos:
        for r in movimentos:
            ref = r[0]
            valor = float(r[1])
            valor = round(valor, 2)
            valor = locale.currency(valor, grouping=True, symbol=None)
            data = r[2].strftime("%d/%m/%Y")
            msg += f'{ref}:\n'  f'R$ {valor}; {data}\n\n'
    else:
        msg += 'Não há Recebimentos futuros\n'
    msg += '/financeiro: Informações financeiras\n\n'
    msg += '/inicial: Volta para o menu inicial'
    return msg

def msg_pagamentos_vencidos(farm_id, farm_name):
    data = datetime.today().strftime("%d/%m/%Y")
    movimentos = sql_movimentos(farm_id, 'payment', 'open')
    nr = len(movimentos)

    msg = f'{farm_id} - <b>{farm_name}</b>\n'
    msg += f'{data}\n'
    msg += f'Pagamentos {nr} (-) Em Aberto\n'
    msg += 'Referência, Valor, Data\n'
    msg += '--------------------------------------------\n' 
    if movimentos:
        for r in movimentos:
            ref = r[0]
            valor = float(r[1])
            valor = round(valor, 2)
            valor = locale.currency(valor, grouping=True, symbol=None)
            data = r[2].strftime("%d/%m/%Y")
            msg += f'{ref}:\n'  f'(R$ {valor}); {data}\n\n'
    else:
        msg += 'Não há Pagamentos Em Aberto\n'
    msg += '/financeiro: Informações financeiras\n\n'
    msg += '/inicial: Volta para o menu inicial'
    return msg

def msg_recebimentos_vencidos(farm_id, farm_name):
    data = datetime.today().strftime("%d/%m/%Y")
    movimentos = sql_movimentos(farm_id, 'receipt', 'open')
    nr = len(movimentos)

    msg = f'{farm_id} - <b>{farm_name}</b>\n'
    msg += f'{data}\n'
    msg += f'Recebimentos {nr} (+) Em Aberto\n'
    msg += 'Referência, Valor, Data\n'
    msg += '--------------------------------------------\n' 
    if movimentos:
        for r in movimentos:
            ref = r[0]
            valor = float(r[1])
            valor = round(valor, 2)
            valor = locale.currency(valor, grouping=True, symbol=None)
            data = r[2].strftime("%d/%m/%Y")
            msg += f'{ref}:\n'  f'R$ {valor}; {data}\n\n'
    else:
        msg += 'Não há Recebimentos Em Aberto\n'
    msg += '/financeiro: Informações financeiras\n\n'
    msg += '/inicial: Volta para o menu inicial'
    return msg
