import json
import requests

from datetime import datetime, timedelta
from queries.app_queries import sql_atividades_resumo
from queries.phone_numbers_query import sql_phone_numbers


def msg_app_pendentes(farm_id, farm_name):

    data = datetime.today().strftime("%d/%m/%Y")

    condicao = "AND ha.status = 'Em Aberto'"
    items = sql_atividades_resumo(farm_id, condicao)

    msg = f'{farm_id} - <b>{farm_name}</b>\n'
    msg += f'{data}\n'
    msg += 'Atividades Em Aberto\n'
    msg += 'Tipo:\n (quantidade); Data mais antiga\n'
    msg += '--------------------------------------------\n' 
    if items:
        for item in items:
            atividade = item[0]
            quantidade = item[1]
            data_mais_antiga = item[2].strftime("%d/%m/%Y")

            msg += f'{atividade}:\n ({quantidade}); {data_mais_antiga}\n\n'
    else:
        msg += 'Não há Atividades Pendentes\n\n'

    msg += '/app: Atividades do aplicativo\n\n'
    msg += '/inicial: Volta para o menu inicial'

    return msg

def msg_app_aprovadas(farm_id, farm_name):

    data = datetime.today()

    data_ref = data - timedelta(days=30)
    data_ref = data_ref.strftime("%Y-%m-%d")

    condicao = "AND ha.status = 'Aprovado'\n"
    condicao += f"AND ha.date::date > '{data_ref}'"

    items = sql_atividades_resumo(farm_id, condicao)
    data = data.strftime("%d/%m/%Y")

    msg = f'{farm_id} - <b>{farm_name}</b>\n'
    msg += f'{data}\n'
    msg += 'Atividades Aprovadas nos últimos 30 dias\n'
    msg += 'Tipo:\n (quantidade); Data mais antiga\n'
    msg += '--------------------------------------------\n' 
    if items:
        for item in items:
            atividade = item[0]
            quantidade = item[1]
            data_mais_antiga = item[2].strftime("%d/%m/%Y")

            msg += f'{atividade}:\n ({quantidade}); {data_mais_antiga}\n\n'
    else:
        msg += 'Não há Atividades Aprovadas nos últimos 30 dias\n\n'

    msg += '/app: Atividades do aplicativo\n\n'
    msg += '/inicial: Volta para o menu inicial'

    return msg


def msg_app_reprovadas(farm_id, farm_name):

    data = datetime.today()

    data_ref = data - timedelta(days=30)
    data_ref = data_ref.strftime("%Y-%m-%d")

    condicao = "AND ha.status = 'Reprovado'\n"
    condicao += f"AND ha.date::date > '{data_ref}'"

    items = sql_atividades_resumo(farm_id, condicao)
    data = data.strftime("%d/%m/%Y")

    msg = f'{farm_id} - <b>{farm_name}</b>\n'
    msg += f'{data}\n'
    msg += 'Atividades Reprovadas nos últimos 30 dias\n'
    msg += 'Tipo:\n (quantidade); Data mais antiga\n'
    msg += '--------------------------------------------\n'
    if items:
        for item in items:
            atividade = item[0]
            quantidade = item[1]
            data_mais_antiga = item[2].strftime("%d/%m/%Y")

            msg += f'{atividade}:\n ({quantidade}); {data_mais_antiga}\n\n'
    else:
        msg += 'Não há Atividades Reprovadas nos últimos 30 dias\n\n'

    msg += '/app: Atividades do aplicativo\n\n'
    msg += '/inicial: Volta para o menu inicial'

    return msg

