import json
import requests
import locale
from datetime import datetime
from queries.estoque_queries import sql_saldos_estoque

locale.setlocale(locale.LC_ALL, 'pt_BR.UTF-8')


def msg_acima_do_minimo(farm_id, farm_name):

    condicao = 'AND item."minimumStock" < sb.quantity'
    items = sql_saldos_estoque(farm_id, condicao)
    data = datetime.today().strftime("%d/%m/%Y")

    msg = f'{farm_id} - <b>{farm_name}</b>\n'
    msg += f'{data}\n'
    msg += 'Items acima do estoque mínimo\n'
    msg += '--------------------------------------------\n'
    msg += 'Item:\n  unidade; (mínimo); Saldo\n'
    msg += '--------------------------------------------\n'
    if items:
        for item in items:
            name = item[0]
            unidade = item[1]
            minimo = item[2]
            saldo = float(item[3])
            saldo = round(saldo, 2)
            saldo = locale.currency(saldo, grouping=True, symbol=None)

            msg += f'{name}:\n {unidade}; ({minimo});  {saldo}\n\n'
    else:
        msg += 'Não items com saldo\n\n'

    msg += '/estoque: Situação dos estoque\n\n'
    msg += '/inicial: Volta para o menu inicial'

    return msg

def msg_abaixo_do_minimo(farm_id, farm_name):

    condicao = 'AND item."minimumStock" > sb.quantity\n'
    condicao += 'AND sb.quantity > 0'
    items = sql_saldos_estoque(farm_id, condicao)
    data = datetime.today().strftime("%d/%m/%Y")

    msg = f'{farm_id} - <b>{farm_name}</b>\n'
    msg += f'{data}\n'
    msg += 'Items abaixo do estoque mínimo\n'
    msg += '--------------------------------------------\n'
    msg += 'Item:\n  unidade; (mínimo); Saldo\n'
    msg += '--------------------------------------------\n'
    if items:
        for item in items:
            name = item[0]
            unidade = item[1]
            minimo = item[2]
            saldo = float(item[3])
            saldo = round(saldo, 2)
            saldo = locale.currency(saldo, grouping=True, symbol=None)

            msg += f'{name}:\n {unidade}; ({minimo});  {saldo}\n\n'
    else:
        msg += 'Não há items com saldo abaixo do mínimo\n\n'

    msg += '/estoque: Situação dos estoque\n\n'
    msg += '/inicial: Volta para o menu inicial'

    return msg


def msg_sem_saldo(farm_id, farm_name):
    condicao = 'AND sb.quantity <= 0'
    items = sql_saldos_estoque(farm_id, condicao)
    data = datetime.today().strftime("%d/%m/%Y")

    msg = f'{farm_id} - <b>{farm_name}</b>\n'
    msg += f'{data}\n'
    msg += 'Items sem saldo\n'
    msg += '--------------------------------------------\n'
    msg += 'Item:\n  unidade;  Saldo\n'
    msg += '--------------------------------------------\n'
    if items:
        for item in items:
            name = item[0]
            unidade = item[1]
            saldo = float(item[3])
            saldo = round(saldo, 2)
            saldo = locale.currency(saldo, grouping=True, symbol=None)

            msg += f'{name}:\n {unidade}; {saldo}\n\n'
    else:
        msg += 'Não items sem saldo\n\n'

    msg += '/estoque: Situação dos estoque\n\n'
    msg += '/inicial: Volta para o menu inicial'

    return msg