import time
from queries.phone_numbers_query import sql_phone_numbers


def verificar_usuario(phone_number, user_id, farm_id=None) -> dict:
    contexto = {}
    resultado = sql_phone_numbers(phone_number)

    if resultado:
        fazendas = list(resultado.get(phone_number).keys())
        if farm_id is None:
            farm_id = fazendas[0]
        farm_name =  resultado.get(phone_number).get(farm_id)['farm']
        menus = resultado.get(phone_number).get(farm_id)['menu']
        contexto = {
            'phone': phone_number,
            'chat_id': user_id,
            'farm_id': farm_id,
            'farm_name': farm_name,
            'menus': menus,
            'time': time.time(),
            'fazendas': fazendas
        }
    return contexto
