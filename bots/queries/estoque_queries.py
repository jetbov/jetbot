
from config import DB_CONFIG
from external_systems.data_source.conexao_psql import ConexaoPSQL


def sql_saldos_estoque(farm_id: int, condicao: str) -> list:

    conection = ConexaoPSQL(
        host=DB_CONFIG.get('host'),
        port=DB_CONFIG.get('port'),
        db=DB_CONFIG.get('db'),
        user=DB_CONFIG.get('user'),
        pwd=DB_CONFIG.get('pwd')
    )

    sql = f"""
        SELECT 
            item.name as item,
            item.unit as unidade,
            item."minimumStock" as minimo,
            sb.quantity as saldo
        FROM view_stockbalance as sb
        INNER JOIN finance_and_herd_item as item ON item.id = sb.item_id
        WHERE sb.farm_id = {farm_id}
        {condicao}
        LIMIT 50;
        """

    results = conection.get(sql)

    conection.close()
    return results

