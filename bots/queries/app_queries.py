
from config import DB_CONFIG
from external_systems.data_source.conexao_psql import ConexaoPSQL


def sql_atividades_resumo(farm_id: int, condicao: str) -> list:

    conection = ConexaoPSQL(
        host=DB_CONFIG.get('host'),
        port=DB_CONFIG.get('port'),
        db=DB_CONFIG.get('db'),
        user=DB_CONFIG.get('user'),
        pwd=DB_CONFIG.get('pwd')
    )

    sql = f"""
        WITH atividade as (
            SELECT * FROM 
            (VALUES 
                ('animal-comment', 'Comentário no Animal'),
                ('change-identification', 'Troca Brinco'),
                ('desmame', 'Desmame'),
                ('diagnostico', 'Diagnóstico'),
                ('health', 'Sanitário'),
                ('sanitario', 'Sanitário'),
                ('inseminacao', 'Inseminação'),
                ('new_animal', 'Novo Animal'),
                ('nutrition-herd', 'Nutrição'),
                ('parto', 'Parto'),
                ('perda&morte', 'Perda ou Morte'),
                ('pesagem', 'Pesagem'),
                ('production-order', 'Produção de Ração'),
                ('rfid', 'RFID'),
                ('troca de lote', 'Troca de Lote'),
                ('troca-de-area', 'Troca de Área')
            ) AS t (cod, tipo) )
        SELECT
            atividade.tipo,
            COUNT(*)as quantidade,
            min(ha.date)::date data_mais_antiga
        FROM api_handleactivity as ha
        LEFT JOIN atividade ON atividade.cod = ha.activity_type
        WHERE ha.farm_id = {farm_id}
            AND ha.deleted = false
            {condicao}
        GROUP BY ha.activity_type, atividade.tipo
        ORDER BY min(ha.date)::date
        LIMIT 50;
        """

    results = conection.get(sql)

    conection.close()
    return results