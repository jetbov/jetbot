
from config import DB_CONFIG
from external_systems.data_source.conexao_psql import ConexaoPSQL


def sql_saldo(farm_id: int) -> list:

    conection = ConexaoPSQL(
        host=DB_CONFIG.get('host'),
        port=DB_CONFIG.get('port'),
        db=DB_CONFIG.get('db'),
        user=DB_CONFIG.get('user'),
        pwd=DB_CONFIG.get('pwd')
    )

    sql = f"""
        SELECT ac.name, sum("valueSign")::float as balance
            FROM finance_and_herd_financetransaction as fin
        INNER JOIN finance_and_herd_financeaccount as ac
            ON ac.id = fin.account_id
        WHERE fin.farm_id = {farm_id} AND fin.deleted = false
        AND date::date <= now()::date
        GROUP BY ac.name;
        """

    results = conection.get(sql)

    data = []
    for row in results:
        dicio = {}
        dicio['account_name'] = row[0]
        dicio['balance'] = row[1]
        data.append(dicio)

    conection.close()
    return data


def sql_movimentos(farm_id: int, tipo: str, status: str = None) -> list:

    conection = ConexaoPSQL(
        host=DB_CONFIG.get('host'),
        port=DB_CONFIG.get('port'),
        db=DB_CONFIG.get('db'),
        user=DB_CONFIG.get('user'),
        pwd=DB_CONFIG.get('pwd')
    )
    data_referencia =  'AND date::date >= now()::date'
    tipo_referencia= ''
    if status:
        data_referencia =  'AND date::date <= now()::date'
        tipo_referencia = " AND paid = false"

    sql = f"""
        SELECT "ref", "value", "date"
            FROM finance_and_herd_financetransaction as fin
        WHERE fin.farm_id = {farm_id} 
        AND fin.deleted = false
        AND "type" = '{tipo}'
        {data_referencia}
        {tipo_referencia}
        ORDER BY date LIMIT 10
        """

    results = conection.get(sql)

    conection.close()
    return results

