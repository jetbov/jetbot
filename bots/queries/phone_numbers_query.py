
from config import DB_CONFIG
from external_systems.data_source.conexao_psql import ConexaoPSQL

def sql_farms(phone_number: str) -> list:

    conection = ConexaoPSQL(
        host=DB_CONFIG.get('host'),
        port=DB_CONFIG.get('port'),
        db=DB_CONFIG.get('db'),
        user=DB_CONFIG.get('user'),
        pwd=DB_CONFIG.get('pwd')
    )

    sql = f"""
    SELECT farm_id
    FROM jetbot_phonefarmmenu 
    WHERE mobile_number = {farm_id};
    """
    results = conection.get(sql)

    conection.close()
    return results


def sql_phone_numbers(phone_number: str) -> list:

    conection = ConexaoPSQL(
        host=DB_CONFIG.get('host'),
        port=DB_CONFIG.get('port'),
        db=DB_CONFIG.get('db'),
        user=DB_CONFIG.get('user'),
        pwd=DB_CONFIG.get('pwd')
    )

    sql = """
    SELECT
        phone.mobile_number,
        phone.farm_id,
        farm.name,
        array_agg(menu.name) as menus,
        phone.name,
        phone.email
    FROM jetbot_phonefarmmenu as phone
        INNER JOIN jetbot_phonefarmmenu_menu 
            AS phone_menu on phone_menu.phonefarmmenu_id = phone.id
        INNER JOIN jetbot_menu
            AS menu ON phone_menu.menu_id = menu.id
        INNER JOIN api_farm as farm
            ON farm.id = phone.farm_id
    WHERE phone.mobile_number = '{phone_number}'
        AND farm.deleted = false
        AND menu.deleted = false
        AND phone.deleted = false
    GROUP BY phone.mobile_number, phone.farm_id,
        phone.name, phone.email, farm.name;
    """.format(phone_number=phone_number)

    results = conection.get(sql)

    data = {}
    for row in results:
        if data.get(row[0]):
            data.get(row[0]).update({row[1]: {'farm': row[2], 'menu': row[3], 'nome': row[4], 'email': row[5]}})
        else:
            data[row[0]] = {row[1]: {'farm': row[2], 'menu': row[3], 'nome': row[4], 'email': row[5]}}
    conection.close()
    return data
