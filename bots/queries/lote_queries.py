
from config import DB_CONFIG
from external_systems.data_source.conexao_psql import ConexaoPSQL


def sql_cotacao(data: str = None) -> tuple:

    conection = ConexaoPSQL(
        host=DB_CONFIG.get('host'),
        port=DB_CONFIG.get('port'),
        db=DB_CONFIG.get('db'),
        user=DB_CONFIG.get('user'),
        pwd=DB_CONFIG.get('pwd')
    )

    data_referencia = ''
    if data:
        data_referencia = f"AND quote_date = '{data}'"
    sql=f"""
    SELECT quote_date, quote_price
        FROM api_esaqcattlequote
        {data_referencia}
    ORDER BY id desc limit 1;
    """
    results = conection.get(sql)
    conection.close()
    return results

def sql_peso(farm_id: int) -> list:

    conection = ConexaoPSQL(
        host=DB_CONFIG.get('host'),
        port=DB_CONFIG.get('port'),
        db=DB_CONFIG.get('db'),
        user=DB_CONFIG.get('user'),
        pwd=DB_CONFIG.get('pwd')
    )

    sql = f"""
        WITH sub as (
            SELECT  herd.id, herd.name, sum(1) as nr_animais,
                SUM(last_wh.weight) as peso_kg
                FROM api_animal as animal
                INNER JOIN api_herd as herd ON animal.herd_id = herd.id
                LEFT JOIN LATERAL (
                    SELECT date, weight
                    FROM api_weighthistory
                    WHERE deleted = false
                    AND animal_id = animal.id
                    ORDER BY date desc limit 1
                ) as last_wh on true
                WHERE animal.farm_id = {farm_id}
                    AND animal.deleted = false 
                    AND animal.sold = false 
                    AND animal.dead = false 
                    and animal.lost = false
                GROUP BY herd.name,  herd.id
            )
        SELECT id, name, nr_animais, peso_kg,
            CASE WHEN peso_kg > 0 THEN peso_kg / 30 ELSE 0 END as peso_arroba,
            CASE WHEN peso_kg > 0 THEN peso_kg / nr_animais ELSE 0 END as peso_medio
        from sub ORDER BY peso_medio DESC
        limit 50;
        """

    results = conection.get(sql)

    conection.close()
    return results

