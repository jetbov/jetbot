
import sys
import os
import logging

import telebot
from telebot import types

from config import (
    API_KEY,
)

from writers.app import (
    msg_app_aprovadas,
    msg_app_pendentes,
    msg_app_reprovadas
)

from writers.lotes import (
    msg_pesos
)

from writers.estoque import (
    msg_abaixo_do_minimo,
    msg_acima_do_minimo,
    msg_sem_saldo
)

from writers.financeiro import (
    msg_pagamentos_vencidos,
    msg_prx_recebimentos,
    msg_prx_pagamentos,
    msg_recebimentos_vencidos,
    msg_saldo
)

from writers.contexto import (
    verificar_usuario
)


bot = telebot.TeleBot(API_KEY, parse_mode='HTML')

logger = telebot.logger

autenticated = {}

def cant_access(message):
    msg = 'Entre em contato com o administrador da sua Fazenda, para liberar seu acesso'
    bot.send_message(
        message.from_user.id,
        msg
    )

@bot.message_handler(commands=["saldo"])
def saldo_das_contas(message):

    context = autenticated.get(message.from_user.id)
    menus = context.get('menus') or []

    if context and 'finance_menu' in menus:
        farm_name = context.get('farm_name')
        farm_id = context['farm_id']
        if farm_id:
            msg = msg_saldo(farm_id, farm_name)
    else:
        msg = 'Não consegui identificar sua fazenda,\n'
        msg += 'pode se autenticar novamente? /start'
    bot.send_message(
        message.chat.id,
        msg
    )

@bot.message_handler(commands=["prx_pagamentos"])
def proximos_pagamentos(message):

    context = autenticated.get(message.from_user.id)
    menus = context.get('menus') or []

    if context and 'finance_menu' in menus:
        farm_name = context.get('farm_name')
        farm_id = context['farm_id']
        if farm_id:
            msg = msg_prx_pagamentos(farm_id, farm_name)
    else:
        msg = 'Não consegui identificar sua fazenda,\n'
        msg += 'pode se autenticar novamente? /start'
    bot.send_message(
        message.chat.id,
        msg
    )

@bot.message_handler(commands=["prx_recebimentos"])
def proximos_recebimentos(message):

    context = autenticated.get(message.from_user.id)
    menus = context.get('menus') or []

    if context and 'finance_menu' in menus:
        farm_name = context.get('farm_name')
        farm_id = context['farm_id']
        if farm_id:
            msg = msg_prx_recebimentos(farm_id, farm_name)
    else:
        msg = 'Não consegui identificar sua fazenda,\n'
        msg += 'pode se autenticar novamente? /start'
    bot.send_message(
        message.chat.id,
        msg
    )

@bot.message_handler(commands=["pagatmentos_vencidos"])
def pagatmentos_vencidos(message):

    context = autenticated.get(message.from_user.id)
    menus = context.get('menus') or []

    if context and 'finance_menu' in menus:
        farm_name = context.get('farm_name')
        farm_id = context['farm_id']
        if farm_id:
            msg = msg_pagamentos_vencidos(farm_id, farm_name)
    else:
        msg = 'Não consegui identificar sua fazenda,\n'
        msg += 'pode se autenticar novamente? /start'
    bot.send_message(
        message.chat.id,
        msg
    )

@bot.message_handler(commands=["recebimentos_vencidos"])
def recebimentos_vencidos(message):

    context = autenticated.get(message.from_user.id)
    menus = context.get('menus') or []

    if context and 'finance_menu' in menus:
        farm_name = context.get('farm_name')
        farm_id = context['farm_id']
        if farm_id:
            msg = msg_recebimentos_vencidos(farm_id, farm_name)
    else:
        msg = 'Não consegui identificar sua fazenda,\n'
        msg += 'pode se autenticar novamente? /start'
    bot.send_message(
        message.chat.id,
        msg
    )

@bot.message_handler(commands=["financeiro"])
def menu_financeiro(message):
    context = autenticated.get(message.from_user.id)
    menus = context.get('menus') or []

    if context and 'finance_menu' in menus:
        menu = """
        {first_name}, sobre o que você quer saber:
        /saldo: Sado da(s) conta(s)\n
        /prx_pagamentos: 10 próximos pagamentos em Aberto\n
        /prx_recebimentos: 10 próximos recebimentos em Aberto\n
        /pagatmentos_vencidos: Valor total dos Pagamentos vencidos em Aberto\n
        /recebimentos_vencidos: Valor total dos Recebimentos vencidos em Aberto\n\n
        /inicial: Volta para o menu inicial
        Clique em uma das opções
        """.format(first_name=message.from_user.first_name)
        bot.reply_to(message, menu)
    else:
        msg = 'Não consegui identificar sua fazenda,\n'
        msg += 'pode se autenticar novamente? /start'
        bot.send_message(
            message.from_user.id,
            msg
        )

@bot.message_handler(commands=["abaixo_minimo"])
def items_com_saldo_abaixo_do_minimo(message):

    context = autenticated.get(message.from_user.id)
    menus = context.get('menus') or []

    if context and 'stock_menu' in menus:
        farm_name = context.get('farm_name')
        farm_id = context['farm_id']
        if farm_id:
            msg = msg_abaixo_do_minimo(farm_id, farm_name)
    else:
        msg = 'Não consegui identificar sua fazenda,\n'
        msg += 'pode se autenticar novamente? /start'
    bot.send_message(
        message.chat.id,
        msg
    )

@bot.message_handler(commands=["acima_do_minimo"])
def items_com_saldo_acima_do_minimo(message):

    context = autenticated.get(message.from_user.id)
    menus = context.get('menus') or []

    if context and 'stock_menu' in menus:
        farm_name = context.get('farm_name')
        farm_id = context['farm_id']
        if farm_id:
            msg = msg_acima_do_minimo(farm_id, farm_name)
    else:
        msg = 'Não consegui identificar sua fazenda,\n'
        msg += 'pode se autenticar novamente? /start'
    bot.send_message(
        message.chat.id,
        msg
    )

@bot.message_handler(commands=["sem_saldo"])
def items_sem_saldo_no_estoque(message):

    context = autenticated.get(message.from_user.id)
    menus = context.get('menus') or []

    if context and 'stock_menu' in menus:
        farm_name = context.get('farm_name')
        farm_id = context['farm_id']
        if farm_id:
            msg = msg_sem_saldo(farm_id, farm_name)
    else:
        msg = 'Não consegui identificar sua fazenda,\n'
        msg += 'pode se autenticar novamente? /start'
    bot.send_message(
        message.chat.id,
        msg
    )

@bot.message_handler(commands=["estoque"])
def menu_estoque(message):
    context = autenticated.get(message.from_user.id) or {}
    menus = context.get('menus') or []

    if context and 'stock_menu' in menus:
        menu = """
        Sobre o que você quer saber:
        /sem_saldo: Items sem saldos no estoque\n
        /abaixo_minimo: Items com saldo abaixo do mínimo\n
        /acima_do_minimo: Items com saldo acima do mínimo\n\n
        /inicial: Volta para o menu inicial
        Clique em uma das opções
        """
        bot.reply_to(message, menu)
    else:
        msg = 'Não consegui identificar sua fazenda,\n'
        msg += 'pode se autenticar novamente? /start'
        bot.send_message(
            message.from_user.id,
            msg
        )

@bot.message_handler(commands=["pesos"])
def peso_cotacao_dos_lotes(message):

    context = autenticated.get(message.from_user.id)
    if context:
        farm_name = context.get('farm_name')
        farm_id = context['farm_id']
        if farm_id:
            msg = msg_pesos(farm_id, farm_name)
    else:
        msg = 'Não consegui identificar sua fazenda,\n'
        msg += 'pode se autenticar novamente? /start'
    bot.send_message(
        message.chat.id,
        msg
    )

@bot.message_handler(commands=["lotes"])
def menu_lotes(message):
    context = autenticated.get(message.from_user.id) or {}
    menus = context.get('menus') or []

    if context and 'stock_menu' in menus:
        menu = """
        Sobre o que você quer saber sobre seus Lotes:
        /pesos: Peso e cotação dos Lotes\n\n
        /inicial: Volta para o menu inicial
        Clique em uma das opções
        """
        bot.reply_to(message, menu)
    else:
        msg = 'Não consegui identificar sua fazenda,\n'
        msg += 'pode se autenticar novamente? /start'
        bot.send_message(
            message.chat.id,
            msg
        )

@bot.message_handler(commands=["pendentes"])
def manejos_pendentes_do_app(message):

    context = autenticated.get(message.from_user.id)
    if context:
        farm_name = context.get('farm_name')
        farm_id = context['farm_id']
        if farm_id:
            msg = msg_app_pendentes(farm_id, farm_name)
    else:
        msg = 'Não consegui identificar sua fazenda,\n'
        msg += 'pode se autenticar novamente? /start'
    bot.send_message(
        message.chat.id,
        msg
    )

@bot.message_handler(commands=["aprovadas"])
def manejos_aprovados_do_app(message):

    context = autenticated.get(message.from_user.id)
    if context:
        farm_name = context.get('farm_name')
        farm_id = context['farm_id']
        if farm_id:
            msg = msg_app_aprovadas(farm_id, farm_name)
    else:
        msg = 'Não consegui identificar sua fazenda,\n'
        msg += 'pode se autenticar novamente? /start'
    bot.send_message(
        message.chat.id,
        msg
    )

@bot.message_handler(commands=["reprovaddas"])
def manejos_reprovados_do_app(message):

    context = autenticated.get(message.from_user.id) or {}
    menus = context.get('menus') or []

    if context and 'app_menu' in menus:
        farm_name = context.get('farm_name')
        farm_id = context['farm_id']
        if farm_id:
            msg = msg_app_reprovadas(farm_id, farm_name)
    else:
        msg = 'Não achei o que você pediu,\n'
        msg += 'pode se autenticar novamente? /start'
    bot.send_message(
        message.chat.id,
        msg
    )

@bot.message_handler(commands=["app"])
def menu_manejos_do_app(message):
    context = autenticated.get(message.from_user.id)
    menus = context['menus']
    if 'app_menu' in menus:
        menu = """
        Sobre o que você quer saber:
        /aprovadas: Atividades Aprovadas nos últimos 30 dias\n
        /reprovaddas: Atividades Reprovadas nos últimos 30 dias\n
        /pendentes: Total de atividades pendentes\n\n
        /inicial: Volta para o menu inicial
        Clique em uma das opções
        """
        bot.reply_to(message, menu)
    else:
        cant_access(message)


@bot.callback_query_handler(func=lambda call: True)
def finance_options(message):
    context = autenticated.get(message.from_user.id)
    menus = context['menus']

    if autenticated.get(message.from_user.id):
        contexto = verificar_usuario(
            phone_number=autenticated.get(message.from_user.id).get('phone'),
            user_id=message.from_user.id,
            farm_id=int(message.data)
        )

        if contexto:
            autenticated[message.from_user.id] = contexto
        else:
            cant_access(message)

    if autenticated.get(message.from_user.id) and contexto:
        menu_inicial(message)


@bot.message_handler(commands=["trocar_fazenda"])
def escolher_fazenda(message):
    context = autenticated.get(message.from_user.id)
    if context:
        fazendas = context.get('fazendas')
        first_name=message.from_user.first_name
        menus = context['menus']
        farm_name = context['farm_name']
        farm_id = context['farm_id']

        msg = f'Fazenda atual: <b>{farm_name}</b>\n'
        msg += f'<b>{first_name}</b> escolha outra Fazenda:\n'

        fazendas.remove(farm_id)
        markup = types.InlineKeyboardMarkup()
        for farm_pk in fazendas:
            markup.row(
                    types.InlineKeyboardButton(
                        text=f"{farm_pk}",
                        callback_data=f"{farm_pk}",
                    )
            )
        msg = bot.send_message(
            message.from_user.id,
            msg,
            reply_markup=markup
        )
    else:
        msg = 'Não consegui identificar sua fazenda,\n'
        msg += 'pode se autenticar novamente? /start'
        bot.send_message(
            message.chat.id,
            msg
        )

@bot.message_handler(commands=["inicial"])
def menu_inicial(message):

    context = autenticated.get(message.from_user.id)
    if context:
        fazendas = context.get('fazendas')
        first_name=message.from_user.first_name
        menus = context['menus']
        farm_name = context['farm_name']
        farm_id = context['farm_id']

        menu = f'<b>{farm_name}</b>\n'
        menu += f'<b>{first_name}</b> aqui está o nosso menu inicial:\n\n'

        opcoes = {
            'finance_menu': '/financeiro: Informações financeiras\n\n',
            'stock_menu': '/estoque: Informações sobre seu estoque\n\n',
            'herd_menu': '/lotes: Situação dos lotes\n\n',
            'app_menu': '/app: Atividades do aplicativo\n\n'
        }
        for item in menus:
            menu += opcoes.get(item)
        if len(fazendas) > 1:
            menu += '/trocar_fazenda: Escolher outra Fazenda'
        menu += '\nClique em uma das opções'

        bot.send_message(
            message.from_user.id,
            menu
        )
        bot.send_message(message.from_user.id, 'Menu inicial') 
    else:
        msg = 'Não consegui identificar sua fazenda,\n'
        msg += 'pode se autenticar novamente? /start'
        bot.send_message(
            message.from_user.id,
            msg
        )


@bot.message_handler(content_types=["contact"])
def contact_handler(message):
    contexto = {}
    if not autenticated.get(message.contact.user_id):
        contexto = verificar_usuario(
            message.contact.phone_number,
            message.contact.user_id
        )

        if contexto:
            autenticated[message.contact.user_id] = contexto
        else:
            cant_access(message)

    if autenticated.get(message.contact.user_id) and contexto:
        menu_inicial(message)


def autenticar(message):
    contexto = {}
    if message.contact and autenticated.get(message.from_user.id) is None:
        '''
        todo:
        * gravar o chat_id na tabela do jetbov
        '''
        contexto = verificar_usuario(
            message.contact.phone_number,
            message.contact.user_id
        )
        if contexto:
            autenticated[message.contact.user_id] = contexto
            return True
        else:
            cant_access(message)
            return False
    elif autenticated.get(message.from_user.id):
        return True
    else:
        return True


@bot.message_handler(func=autenticar)
def start(message):
    if autenticated.get(message.chat.id) is None:
        markup = types.ReplyKeyboardMarkup(
            row_width=1,
            resize_keyboard=True
        )
        button_phone = types.KeyboardButton(
            text="enviar o telefone",
            request_contact=True
        )
        markup.add(button_phone)
        bot.send_message(
            message.chat.id,
            'Clique no botão "enviar o telefone" para iniciar',
            reply_markup=markup
        )
    elif phone_numbers.get(autenticated.get(message.from_user.id)['phone']) is None:
        msg = 'Entre em contato com o administrador da sua Fazenda, para liberar seu acesso'
        bot.send_message(
            message.chat.id,
            msg
        )

    else:
        menu = """
        Como posso te ajudar:
        /financeiro: Informações financeiras\n
        /estoque: Informações sobre seu estoque\n
        /lotes: Situação dos lotes\n
        /app: Atividades do aplicativo\n
        Clique em uma das opções
        """
        bot.send_message(
            message.chat.id,
            menu
        )

telebot.logger.setLevel(logging.DEBUG)

bot.infinity_polling()
