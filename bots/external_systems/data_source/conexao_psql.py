import psycopg2
from gateways.sql_gateway import ConexaoSQL


class ConexaoPSQL(ConexaoSQL):
    _db=None
    def __init__(self, host, port, db, user, pwd):
        self._db = psycopg2.connect(
            host=host,
            port=port,
            database=db,
            user=user,
            password=pwd
        )

    def post(self, sql):
        ...
        # try:
        #     cursor = self._db.cursor()
        #     cursor.execute(sql)
        #     cursor.close();
        #     self._db.commit()
        # except Exception as e:
        #     raise e.message
        # return True

    def get(self, sql):
        result = None
        try:
            cursor=self._db.cursor()
            cursor.execute(sql)
            result = cursor.fetchall();
        except Exception as e:
            raise e
        return result

    def close(self):
        self._db.close()