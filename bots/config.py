import os

API_KEY = os.getenv('TELEGRAM_API_KEY')

DB_CONFIG = {
    'host': os.getenv('DB_IP'),
    'port': os.getenv('DB_PORT'),
    'db': os.getenv('DB_NAME'),
    'user':os.getenv('DB'),
    'pwd': os.getenv('DB_PWD')
}
