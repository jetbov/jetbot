from abc import ABC, abstractmethod


class ConexaoSQL(ABC):
    _db=None

    @abstractmethod
    def __init__(self, host, port, db, user, pwd):
        '''
        configure sua conexão, ex:
        self._db = psycopg2.connect(
            host=host,
            port=port,
            database=db,
            user=user,
            password=pwd
        )
        '''
    @abstractmethod
    def post(self, sql):
        raise NotImplementedError

    @abstractmethod
    def get(self, sql):
        raise NotImplementedError

    @abstractmethod
    def close(self):
        raise NotImplementedError
